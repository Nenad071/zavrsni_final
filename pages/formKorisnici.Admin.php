<?php
include "procesess/processKorisnici.Admin.php";
$statData = $currentUser === null || (empty($currentUser));
?>

<div class="9u mobileUI-main-content">
    <div id="content">
        <p style="width:250px;margin:50px 330px;font-weight:bold;color:red;"> ADMIN/Korisnici </p>
         <form class="form-style-9" style="margin:0px auto;" action="" method="POST" >  
<?php 
    if (isset($stringError))
    echo $stringError;
    
    if (!empty($messages))
    {
        foreach($messages as $msg)
        {
            echo "<span style='color:red'>{$msg}</span><br>";
        }
    }
?>
            <ul>
                <li>
                    <input class="field-style field-split align-left" type="text" name="search" value ="<?= $statData ? "" : $currentUser[0]->korisnickoIme; ?>">
                    <input class="field-style field-split align-right" type="submit"  name="trazi" value="PRETRAGA">
                </li> 
                <li>
                    <input type="hidden" name="idUpdate" value="<?= $statData ? "" : $currentUser[0]->idUser ?>">
                    <input type="text" name="ime" value="<?= $statData ? "" : $currentUser[0]->ime ?>" class="field-style field-split align-left" placeholder="Ime">
                    <input type="text" name="prezime" value="<?= $statData ? "" : $currentUser[0]->prezime ?>" class="field-style field-split align-right" placeholder="Prezime">

                </li>
                <li>
                    <input type="text" name="korisnickoIme" value="<?= $statData ? "" : $currentUser[0]->korisnickoIme ?>" class="field-style field-split align-left" placeholder="Korisničko Ime">
                    <input type="email" name="email" value="<?= $statData ? "" : $currentUser[0]->email ?>" class="field-style field-split align-right" placeholder="Email">
                </li>
                <li>
                    <input type="password" name="sifra" value="<?= $statData ? "" : $currentUser[0]->sifra ?>" class="field-style field-split align-left" placeholder="Unesite Sifru">
                </li>

        Status: <br>

<?php
    $pristup = new NivoPristupa();
    $pristupi = $pristup->GetAll();
?>
                <li> 
                    <select class="field-style field-full align-none" name="idPristupa">
                        <option value="-1">Izaberite Status</option>

<?php
    foreach ($pristupi as $pr)
    {
        $selected = "";
        $selected = ($currentUser[0]->idPristupa == $pr->idPristupa) ? "selected" : "";
        echo "<option " . $selected . " value='{$pr->idPristupa}'> {$pr->tip}</option><br> ";
    }
?>
                    </select>
                </li>
                <li>  
                    <input type="submit" name="unesi" value="UNESI">
                    <input type="submit" name="izmeni" value="IZMENI">  
                    <input type="submit" name="obrisi" value="OBRIŠI">
                </li>
            </ul>
        </form>
        
        <div style='width:600px;margin:50px auto 20px;'>
            <button type="button" style='width:100px;margin:50px auto 20px;' id="listBtn">
                Prikazi Listu
            </button>
        
   <?php 
   
        $allUsers  = New User();
        $allUsers->fields="korisnickoIme";
        $allUsers->preparedValues="?";
        $resultAll = $allUsers->GetAll();
        
        echo "<div style='display:none;width:450px;margin:0 auto;' id='list'>";
        echo "<ul>";
        
        foreach($resultAll as $res)
        {
            echo "<li>{$res->korisnickoIme}</li>";
        }
        echo "</ul>";
        echo "</div>";
   ?>     
        </div>
    </div> 
</div>

<script>
    var prikaz = false;
    $("#listBtn").click(function(){
        if(!prikaz)
        {
            $("#list").show();
            prikaz = true;
        }
        else
        {
            $("#list").hide();
            prikaz = false;
        } 
    }) 

</script>






