<?php

$categories = new Category();
$allCat = $categories->GetAll();
$subjects = new Predmeti();
$subjectsData = null;

    $postName = "";
    $postPrice = "";
    $postDesc = "";
    $postCat = "";

$fieldsRules= array
(
    "naziv" => array("required" => "", "min" => "3"),
    "cena" => array("required" => "", "number" => "", "min" => "2"),
    "opis" => array("required" => "", "min" => "50"),
    "idKategorija" => array("required" => "", "minnumber"=>"")
);

if (isset($_GET['predmet']))
{
    $subjects->id = $_GET['predmet'];

    if (isset($_GET['obrisi']))
    {
        $dir = $subjects->GetById();
        $dirname = "predmeti/". $dir->naziv;
        
        $files = glob($dirname.'/*'); 
        foreach($files as $file)
        { 
          if(is_file($file))
            unlink($file); 
        }

        rmdir($dirname);
        $subjects->Delete();
       echo "<script>window.location = 'index.php?page=12'</script>";
    }
}

$subjects->fields = "naziv,slika,materijal,cena,opis,idKategorija";
$subjects->preparedValues = "?,?,?,?,?,?";

if (isset($_POST['unesi']))   
{
    $validation = new Validation($_POST, $fieldsRules);
    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
        $postName = $_POST['naziv'];
        $postPrice = $_POST['cena'];
        $postDesc = $_POST['opis'];
        $postCat = $_POST['idKategorija'];
    } 
    else 
    {
        if(!empty($_POST['naziv']) && is_dir($_POST['naziv'])===false)
        {
            $newDir= mkdir("predmeti/"."{$_POST['naziv']}");
        }
        
        if (isset($_FILES['slika']))
        {
            $subjects->slika = $_POST['naziv'] . "_" . time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);   
        }
        
        if (isset($_FILES['materijal']))
        {
            $finalName = $_FILES['materijal']['name'];
            $tempFile = $_FILES['materijal']['tmp_name'];

            foreach($finalName as $key=>$sub)
            {
                move_uploaded_file($tempFile[$key], "predmeti/".$_POST['naziv']  ."/".$sub);  
                $subjects->materijal[] = $sub; 
            }
        }
       
        $_POST['slika'] = $subjects->slika;
        $_POST['materijal'] = implode("," , $subjects->materijal);
        $subjects->valuesForInsert = $_POST;
        $subjects->Insert();
    }
}

if (isset($_POST['izmeni']))
{
    $validation = new Validation($_POST,$fieldsRules);

    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    } 
    else
    {
        if ($_FILES['slika']['name'] !== "" && $_FILES['materijal']['name'] !== "")
        {   
            $subjects->id = $_GET['predmet'];
            $docName = $subjects->GetById();
            
            $subjects->slika = $_POST['naziv'] . "_" . time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);
            $_POST['slika'] = $subjects->slika;
            
            $finalName = $_FILES['materijal']['name'];
            $tempFile = $_FILES['materijal']['tmp_name'];
            
            $old_dir_name = "predmeti/".$docName->naziv;
            $new_dir_name = "predmeti/".$_POST['naziv'];
            
            $dirname = rename($old_dir_name,$new_dir_name);
 

            foreach($finalName as $key=>$sub)
            {
                move_uploaded_file($tempFile[$key], "predmeti/".$_POST['naziv']  ."/".$sub);  
                $subjects->materijal[] = $sub; 
            }
            
            $_POST['materijal'] = implode("," , $subjects->materijal);
        }

        elseif ($_FILES['slika']['name'] !== "" && $_FILES['materijal']['name'] == "")
        {
            $subjects->fields = "naziv,cena,slika, opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?,?";
            $subjects->slika = $_POST['naziv'] . "_" . time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);
            $_POST['slika'] = $subjects->slika;
        }

        elseif ($_FILES['materijal']['name'] !== "" && $_FILES['slika']['name'] == "" )
        {
            $subjects->fields = "naziv,cena,materijal,opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?,?";
            
            $subjects->id = $_GET['predmet'];
            $docName = $subjects->GetById();
            
            $subjects->slika = $_POST['naziv'] . "_" . time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);
            $_POST['slika'] = $subjects->slika;
            
            $finalName = $_FILES['materijal']['name'];
            $tempFile = $_FILES['materijal']['tmp_name'];
            
            $old_dir_name = "predmeti/".$docName->naziv;
            $new_dir_name = "predmeti/".$_POST['naziv'];
            
            $dirname = rename($old_dir_name,$new_dir_name);
 

            foreach($finalName as $key=>$sub)
            {
                move_uploaded_file($tempFile[$key], "predmeti/".$_POST['naziv']  ."/".$sub);  
                $subjects->materijal[] = $sub; 
            }
            
            $_POST['materijal'] = implode("," , $subjects->materijal);
        }  
        else
        {
            $subjects->fields = "naziv,cena,opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?"; 
        }

        $subjects->id = $_GET['predmet'];
        $subjects->valuesForInsert = $_POST;
        $subjects->Update();        
    }
}

if (isset($_GET['predmet']))
{
    $subjectsData = $subjects->GetById();
    if (isset($_GET['obrisi']))
    {
        $subjectsData = null;
    }
}

$subjects->fields = "idPredmeta,naziv,slika,cena,materijal,opis,idKategorija";
$subjects->preparedValues = "?,?,?,?,?,?,?";

$allSubjects = $subjects->GetAll();