<?php


class AccessLevels
{
    private  $blacklistAcess = array
    (
        "1"=>array("formKategorije.Admin.php","formPredmeti.Admin.php","adminIndex.php", "formKorisnici.Admin.php"),
        "2"=>array("formKategorije.Admin.php"),
        "3"=>array()
    );

    public function __construct($user, $page, $strane)
    {
        $this->user = $user;
        $this->page = $page;
        $this->nizStrana = $strane;   
    }
    
    public function PageAccessDenied()
    {
        $nivoPristupa = 1;
        if (!is_null($this->user))
        {
            $nivoPristupa=$this->user->idPristupa;
        }
        
        if (key_exists($this->page, $this->nizStrana))
        {
            if (in_array($this->nizStrana[$this->page], $this->blacklistAcess[$nivoPristupa]))
            {
                header("Location:?page=4");
                die;
            }
        }
        else
        {
            header("Location:?page=7");
            die;  
        }
    }  
}
