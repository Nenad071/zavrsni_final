<?php

require "../config.php";
$date = date('Y-m-d H:i:s', time());

if (isset($_SESSION['user']))
{
    //$user = User::UnserializeUser();
    $basket = new Basket($user);

    if (!is_object($basket->GetBasketByUser()) && $basket->GetBasketByUser()===false)
    {
        if ($user->korpa!==null && $user->korpa!==false)    
        {
             $basket->InsertIntoBasket();
        }
    }
    else
    {
        if (empty($user->korpa->nizProizvoda))
        {
            $basket->Delete();
        }
        else
        {
            $basketNiz['content']= serialize($user->korpa);
            $basketNiz['datum']= $date;
            $basket->fields = "content,datum";
            $basket->preparedValues = "?,?";
            $basket->valuesForInsert = $basketNiz;
            $basket->Update();
        }
    }    
    $user = User::logout();
    
    header ("Location:"._WEB_PATH.TO_ROOT);
}     
