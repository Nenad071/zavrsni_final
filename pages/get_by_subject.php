<?php

if (isset($_GET['idpredmeta']))
{
    $predmeti = new Predmeti();
    $predmeti->id = $_GET['idpredmeta'];
    $predId = $predmeti->GetById();
    
?>
    <div class="9u mobileUI-main-content">
        <div id="content">
            <section class="heightSec">
                <div class="post">
                    <h2><?= $predId->naziv?></h2>
                    <p class="aboveImg">
                        <b class="cena">Price:</b> 
                        <b class="iznos">$<?= $predId->cena?></b>
                    </p>
                    <p class="opisTab">
                        <a href="#">
                            <img src="files/<?= $predId->slika?>" alt="" class="img-alignleft" style="width:225px;height:300px;">
                        </a>
                <?php 
                    if(!isset($_SESSION['user']))
                    {
                       echo substr($predId->opis,0,958);
                    }
                    else echo $predId->opis;
                ?>
                    </p>
                    <p class="button-style">
                        <a href="javascript:history.back()">Back</a>
                <?php 
                    if(isset($_SESSION['user']))
                    {
                ?>
                        <a href="procesess/dodaj.php?idPredmeta=<?= $predId->idPredmeta?>">Add To Cart</a>
                    </p>
                <?php 

                    }
                ?>
                </div>
            </section>
        </div>
    </div>
<?php 
} 
?>
