<?php

$data = array();
$messages =array();
$fields = array
        (
            "ime" => array("required" => "", "alpha" => "","min"=>"3"),
            "prezime"=>array("required" => "", "alpha" => "","min"=>"3"),
            "korisnickoIme" => array("required" => "", "alphanumeric" => "","min"=>"4"),
            "email"=> array("required" => "", "email" => ""),
            "sifra" => array("required" => "", "min"=>"4","max"=>"25"),
            "sifra_confirm" => array ("confirm" => ""),
        );

if (isset($_POST['unesi']))
{
    $validation = new Validation($_POST, $fields);
    $result = $validation ->validate();

    if ($result["error"]===true)
    {
        $stringError = "";
        foreach($result["messages"] as $k=>$v)
        {
            foreach($v as $key=>$value)
            {
                $stringError.= "<span style='color:red'>".$value."</span> <br>";
            }
        }
    }
    else
    {
        $data = $_POST;
        $user = new User();
        
        $username = inputFilter($data["korisnickoIme"]);
        $email    = inputFilter($data["email"]);
        $data['sifra'] = password_hash($data['sifra'], PASSWORD_DEFAULT);

        $user->fields = "korisnickoIme,email";
        $user->preparedValues = "?,?";
        $user->filter = "and korisnickoIme= '{$username}' or email = '{$email}'";
        $result =  $user->GetAll();

        if (count($result)<1)
        {
            $user->fields = "korisnickoIme,ime,prezime,email,sifra";
            $user->valuesForInsert=$data;
            $user->preparedValues = "?,?,?,?,?";

            if ($user->Insert()!==false)
            {
                              
                echo "<script>document.write('Uspesna registracija, uskoro cete biti preusmereni na  stranu za logovanje.');"
                . "setTimeout(function(){ window.location='index.php?page=2' }, 3000);</script>";
                
//                echo "<script>document.write('Uspesna registracija, uskoro cete biti preusmereni na pocetnu stranu.');"
//                . "setTimeout(function(){ window.location='index.php' }, 3000);</script>";
            }
            else
            {
                echo "doslo je do greske";
            }
        }
        else
        {
           foreach ($result as $resul)
           {
                if ($resul->korisnickoIme ==$username)
                {
                    $messages[]= "korisnicko ime je zauzeto. <br>";
                }

                if ($resul->email ==$email)
                {
                    $messages[]= "email je zauzet";
                }
           }
        }
    }
}
