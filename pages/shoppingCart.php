    <?php 

    $mustLogin= '<p style="margin: 100px 150px;color:red;font-size:18px;font-weight: bold;">Morate biti ulogovani da biste dodali u korpu!</p>';
    $emptyBasket= '<p style="margin: 100px 150px;color:red;font-size:18px;font-weight: bold;">Vasa korpa je prazna. </p>';
    $tableLayout = <<< LOUT
<table class="tableLayout">
<tr>
    <th id="del">Delete</th>
    <th id="naz">Naziv</th>
    <th id="opis">Opis</th>
    <th id="cena">Cena</th>
</tr>
LOUT;
    ?>

<div class="9u mobileUI-main-content">
    <div id="content">
<?php

if (!isset($_SESSION['user'])) 
{
     echo $mustLogin;
}
else
{   
    $user= User::UnserializeUser();
    if (empty($user->korpa->nizProizvoda) || is_null($user->korpa))
    {
        echo $emptyBasket;
    }
    else   
    {     
        echo $tableLayout;
       foreach ($user->korpa->nizProizvoda as $us)
       {
    ?>
            <tr>
                <td><a href="<?= PROCESS_DIR; ?>dodaj.php?idPredmeta=<?= $us->idPredmeta?>&obrisi=1">X</a></td>
                <td><?= $us->naziv?></td>
                <td><?= substr($us->opis,0,150)?>...</td>
                <td><?= $us->cena?></td>
            </tr>
     
    <?php 

        }
    ?>  
            <tr>
                <td colspan="3">TOTAL</td>
               <td><?= $user->korpa->Sum()?></td>  
           </tr>
        </table>
        <div class="order">
            <a href="<?= _WEB_PATH.PROCESS_DIR; ?>dodaj.php?ponisti=1">
                <button type="hidden" name="ponisti">Ponisti</button>
            </a>
            <a href="<?= _WEB_PATH.PROCESS_DIR; ?>Order.php">
            <button type="hidden" name="potvrdi">Potvrdi</button>
            </a>
        </div>
<?php
    }
}
?>
  </div>
</div>


