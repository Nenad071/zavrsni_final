<?php

function inputFilter($var)
{
    $var = trim($var);
    if ((int) $var === 0 && is_string($var) && $var !== '0')
    {
       return filter_var($var, FILTER_SANITIZE_SPECIAL_CHARS); 
    }    
    else
    {
        return floatval($var);
    }        
}


function validate($data)
{
    foreach($data as $key => $value)
    {
       $d[$key] = inputFilter($value);
    }
    return $d;
}

