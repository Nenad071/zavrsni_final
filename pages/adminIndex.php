<?php

$users = new User();
$users->fields = "k.idUser,k.korisnickoIme,k.ime, k.prezime, k.email,k.datumPristupa, np.tip";
$users->preparedValues = "?,?,?,?,?,?,?";
$users->table = "korisnici as k";
$users->join = "inner join nivopristupa as np on k.idPristupa = np.idPristupa";
$users->filter = " order by k.datumPristupa desc limit 5";
$listOfUsers = $users->GetAll();

$date = date("d/M/Y H:i:s", strtotime($user->datumPristupa));

$user->datumPristupa = $date; 
#  date("d.m.Y H:i:s", strtotime($user->datumPristupa));

?>

<div class="9u mobileUI-main-content">
    <div id="content">

        <div style="margin:40px 0 40px 70px;">
            <h2>Dobro Dosli <?= $user->korisnickoIme; ?></h2>
            <p>Vase Ime: <?= $user->ime; ?></p>
            <p>Vase Prezime:<?= $user->prezime; ?></p>
            <p>Vase Email:<?= $user->email; ?></p>
            <p>Datum registracije:<?= $user->datumPristupa; ?></p>
        </div>
        
        <div style="margin:40px 0 40px 70px;">
            <h2 style="padding:0px;">Najnoviji korisnici:</h2>

            <table class="tableLayout" style="margin-top:40px;margin-left:0;">
                <tr>
                    <th class="cena">Korisnicko Ime</th>
                    <th class="del">Ime</th>
                    <th class="cena">Prezime</th>
                    <th class="naz">Email</th>
                    <th class="del">Status</th>
                    <th class="cena">Datum Pristupa</th>
                    <th class="del">Izmeni</th>
                </tr>
        <?php 
            foreach ($listOfUsers as $oneUser)
            {
                $dateUsers = date("d/M/Y H:i:s", strtotime($oneUser->datumPristupa));
        ?>
                <tr>
                    <td><?= $oneUser->korisnickoIme; ?></td>
                    <td><?= $oneUser->ime; ?></td>
                    <td><?= $oneUser->prezime; ?></td>
                    <td><?= $oneUser->email; ?></td>
                    <td><?= $oneUser->tip; ?></td>
                    <td><?= $dateUsers; ?></td>
                     <td>
                         <a href="index.php?page=14&idUser=<?= $oneUser->idUser?>" target="_blank">Izmeni</a>
                     </td>
                </tr>
        <?php 
            }
        ?>
            </table>
        </div>
    </div>                
</div>               
