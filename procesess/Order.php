<?php
require "../config.php";
$date = date('Y-m-d H:i:s', time());

if (isset($_SESSION['user']))
{
    $user = User::UnserializeUser();

        $basket = new Basket($user);

            if (!is_object($basket->GetBasketByUser()) && $basket->GetBasketByUser()===false)
            {
                if ($user->korpa!==null && $user->korpa!==false)    
                { 
                     $basket->InsertIntoBasket(2);
                }
            }
            else
            {
                $basketNiz['content']= serialize($user->korpa);
                $basketNiz['status']= 2;
                $basketNiz['datumPorudzbine']= $date;

                $basket->fields = "content,status,datumPorudzbine";
                $basket->preparedValues = "?,?,?";
                $basket->valuesForInsert = $basketNiz;
                $basket->Update();
            }
    // Zipovanje fajlova
    foreach ($user->korpa->nizProizvoda as $niz)
    {
        $rootPath = realpath('../predmeti/'.$niz->naziv);
        $zip = new ZipArchive();
        $zip->open('../archive/'.$niz->naziv. '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            if (!$file->isDir())
            {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
    }
    header("Location:".TO_ROOT."?page=6");   
}


    
    
    
