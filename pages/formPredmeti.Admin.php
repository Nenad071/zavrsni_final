<?php

include 'procesess/processPredmeti.Admin.php';
//include 'procesess/tmp.php';


?>

<div class="9u mobileUI-main-content">
    <div id="content">
<p style="width:250px;margin:50px 330px;font-weight:bold;color:red;"> ADMIN/Predmeti </p>

        <form class="form-style-9" action="" method="post" enctype="multipart/form-data">
    
<?php


if (isset($stringError))
    echo $stringError;
?>
    
            <ul>
                <li>
                    <select class="field-style field-full align-none" onchange="window.location = '?page=12&predmet=' + this.value" name="izbor_predmeta">
                        <option value="-1">Izaberite Predmet</option>

                    
<?php
foreach ($allSubjects as $pred) {
    $selected = "";
    if (isset($_GET['predmet'])) {
        $selected = ($_GET['predmet'] == $pred->idPredmeta) ? "selected" : "";
    }
    echo "<option " . $selected . " value='{$pred->idPredmeta}'> {$pred->naziv}</option><br> ";
}
?>
                    </select> 
                </li>                  
   
<?php
$sData = $subjectsData === null || (empty($subjectsData));
?>
     
                <li>
                    <input type="text" name="naziv" value="<?= $sData ? $postName : $subjectsData->naziv ?>" class="field-style field-split align-left" placeholder="Predmet">
                    <input type="text" name="cena" value="<?= $sData ? $postPrice : $subjectsData->cena ?>" class="field-style field-split align-right" placeholder="Cena">

                </li>

                <li>
                    <textarea name="opis"  class="field-style" placeholder="Opis"><?= $sData ? $postDesc : $subjectsData->opis ?></textarea>
                </li>

                <li>
                    <input type="file" name="slika" value="<?= $sData ? "" : $subjectsData->slika ?>"  class="field-style field-split align-left" placeholder="Slika">
                    <input type="file" multiple name="materijal[]" value="<?= $sData ? "" : $subjectsData->materijal ?>"  class="field-style field-split align-right" placeholder="PDF"> 


                </li>

                <li>

                    <img src="files/<?= $sData ? "" :$subjectsData->slika ?>" class="field-style field-split align-right" alt="" width="75" height="100">
                </li>
                    
                     Izaberite Kategoriju:<br><br>
                <li> 
                    <select  class="field-style field-full align-none" name="idKategorija">
                        <option value="-1">Izaberite Kategoriju</option>
                        
<?php
foreach ($allCat as $cat) {
    $selected = ($subjectsData->idKategorija == $cat->idKategorija) ? " selected" : "";
    echo "<option " . $selected . " value='{$cat->idKategorija}'> {$cat->naziv}</option><br> ";
}
?>
                    </select>
                </li>
                     
                <li>
                    <input type="submit" name="unesi" value="UNESI">
                    <input type="submit" name="izmeni" value="IZMENI">  
                    <input type="button" name="obrisi"  value="OBRISI" onclick="Obrisi()">
                 </li>
            </ul>
        </form>
    </div>
</div>           

<?php 
 
//    INSERT
//    1. setovanje polja u vrhu:
//    $subjects->fields="naziv,slika,cena,opis,idKategorija";
//    $subjects->preparedValues = "?,?,?,?,?";
//    
//    $_POST['slika']=$subjects->slika;
//    $subjects->valuesForInsert=$_POST;
//    $lastId = $subjects->Insert();
//    $subjects->id = $lastId;
//    $subjects->fields="slika";
//    $subjects->preparedValues = "?";
//    $subjects->Update();
    
//    2.  setovanje polja u vrhu
//    $subjects->fields="naziv,slika,cena,opis,idKategorija";
//    $subjects->preparedValues = "?,?,?,?,?";
//       
//    $_POST['slika']=$subjects->slika;
//    $subjects->valuesForInsert['naziv'] = $_POST['naziv'];
//    $subjects->valuesForInsert['slika'] = $_POST['slika']; #$subjects->slika;
//    $subjects->valuesForInsert['cena'] = $_POST['cena'];
//    $subjects->valuesForInsert['opis'] = $_POST['opis'];
//    $subjects->valuesForInsert['idKategorija'] = $_POST['idKategorija'];
//
//     $subjects->Insert();
    
 //      UPDATE 
 //         
//      1. setovanje polja u vrhu
//      $subjects->fields="naziv,slika,cena,opis,idKategorija";
//      $subjects->preparedValues = "?,?,?,?,?";
//      $subjects->id=$_GET['predmet'];
//      $_POST['slika']=$subjects->slika;
//      $subjects->valuesForInsert['naziv'] = $_POST['naziv'];
//      $subjects->valuesForInsert['slika'] = $subjects->slika;
//      $subjects->valuesForInsert['cena'] = $_POST['cena'];
//      $subjects->valuesForInsert['opis'] = $_POST['opis'];
//      $subjects->valuesForInsert['idKategorija'] = $_POST['idKategorija'];

//    $subjects->Update();

?>



<script>
    function Obrisi()
    {
        var potvrda = confirm("Da li ste sigurni da zelite da obrisete ovaj proizvod");
        
        if (potvrda)
        {
            window.location = "?page=12&predmet=<?= $_GET['predmet']?>&obrisi=1";
        }
    }
</script>
