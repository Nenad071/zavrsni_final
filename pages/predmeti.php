<?php

$predmeti = new Predmeti();
$predmeti= $predmeti->GetAll();

$noFetch = "<p>There are no predmeti to fetch!</p>";

$i=0;
?>

<div class="9u mobileUI-main-content">
    <div id="content">
        
<?php 
    if(!empty($predmeti))
    {
        foreach ($predmeti as $pred)
        { 
?>
            <section class="heightSec">
                <div class="post">
                    <h2><?= $pred->naziv?></h2>
                    <p class="aboveImg">
                        <b class="cena">Price:</b> 
                        <b class="iznos">$<?= $pred->cena?></b>
                    </p>
                    <p class="opis">
                        <a href="#">
                            <img src="files/<?= $pred->slika?>" alt="" class="img-alignleft"  style="width:225px;height:300px;">
                        </a><?= substr($pred->opis,0,958)?>...
                    </p>
                    <p class="button-style">
                        <?php 
                            if (isset($_SESSION['user']))
                            {
                         ?>
                        <a href="index.php?page=8&idpredmeta=<?= $pred->idPredmeta ?>">Read Full Article</a>
                        <a href="procesess/dodaj.php?idPredmeta=<?= $pred->idPredmeta?>">Add To Cart</a>
                        <?php 
                            } 
                        ?>
                    </p>
                </div>
            </section>
    <?php 
        $i++;
        }
    }
    else 
    {
        echo $noFetch;
    }
?>
    </div>
</div>


