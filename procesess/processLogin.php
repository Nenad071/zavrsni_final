<?php

$rules = array
(
    "korisnickoIme" => array("required" => "", "alphanumeric" => "","min"=>"4"), 
    "sifra" => array("required" => "", "min"=>"4","max"=>"25")
);

$noUser = false;
$noResult = "<span style = 'color:red;font-weight:bold;'>Pogresno korisnicko ime ili sifra. Pokusajte ponovo!</span>";


if (isset($_POST['submitbtn']))
{
    $validation = new Validation($_POST,$rules);    
    $result = $validation ->validate();

    if ($result["error"]===true)
    {
        $stringError = "";
        foreach($result["messages"] as $k=>$v)
        {
            foreach($v as $key=>$value)
            {
                $stringError.= "<span style = 'color:red;'>". $value." </span><br>";
            }
        }
    }
    else
    {
        $username = inputFilter($_POST['korisnickoIme']);
        $password = $_POST['sifra'];
        
        $user = new User();
        $user->fields = "idUser,idPristupa,korisnickoIme,ime,prezime, sifra,email,datumPristupa";
        $user->preparedValues = "?,?,?,?,?,?,?,?";
        $user->filter = "and korisnickoIme = '{$username}' and idPristupa in (1,2,3)";
        $res = $user->GetAll();
        
        if (count($res)==1)   
        {
            $passwordForCheck = password_verify($password,$res[0]->sifra);

            if ($passwordForCheck===true)
            {
                $result = $user->login();

                if ($result)
                {  
                    $userObject = User::UnserializeUser();

                    if ($userObject->idPristupa==1)
                    {
                        echo "<script>window.location='index.php?page=0';</script>";
                        die;
                        
                    }
                    else
                    {
                        echo "<script>window.location='index.php?page=13';</script>";
                        die;
                      
                    }
                }    
            }
            else $noUser = true;
        }
        else $noUser = true;
    }  
 } 

