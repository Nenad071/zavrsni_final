<?php

class Korpa 
{
  
    public $nizProizvoda=array();

    public function addToCart($proizvod)
    {
        $this->nizProizvoda[]=$proizvod;    
    }
    public function SetSession($korpa)
    {  
        Session::SetSession("korpa", serialize($korpa));
    }
    
    public static function UnserializeKorpa()
    {   
        return unserialize($_SESSION['korpa']);
    }
    
    public function RemoveItem($id)
    {
        $refeshedNiz = array();
        foreach ($this->nizProizvoda as $np)
        {
            if ($np->idPredmeta != $id)
            {
                $refeshedNiz[]=$np;
            }
        }
        $this->nizProizvoda = $refeshedNiz;       
    }
    
    public function Sum ()
    {
        $total = 0;
        foreach ($this->nizProizvoda as $np)
        {
            $total+=$np->cena;
        }
        return $total;
    }
    
       public function DestroyCart()
    {
        $this->nizProizvoda = null;     
        Session :: UnsetSession("korpa");
    } 
}
