<?php


class Category extends Database
{
    public $fields = "idKategorija,naziv,opis";
    public $table = "kategorije";
    public $preparedValues = "?,?,?";
    public $primaryKey = "idKategorija";
    public $filter = "";
}
