<?php

    $postName = "";
    $postDesc = "";

    $fieldsRules= array
    (
        "naziv" => array("required" => "", "min" => "3"),
        "opis" => array("required" => "", "min" => "50"),
    );

if (!isset($_GET['cat']))
{
    $_GET['cat'] = -1;
}

$categories = new Category();
$categoryData = null;

if (isset($_GET['cat']))
{
    $categories->id = $_GET['cat'];

    if (isset($_GET['obrisi']))
    {
        $categories->Delete();
        $predmeti = new Predmeti();
        $predmeti->primaryKey = "idKategorija";

        $predmeti->id = $_GET['cat'];
        $predmeti->Delete();
    }
}

$categories->fields = "naziv,opis";
$categories->preparedValues = "?,?";

if (isset($_POST['unesi'])) 
{
    $validation = new Validation($_POST, $fieldsRules);
    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
        $postName = $_POST['naziv'];
        $postDesc = $_POST['opis'];
    } 
    else
    {
        $categories->valuesForInsert = $_POST;
        $categories->Insert();
    }
}

if (isset($_POST['izmeni'])) 
{
    $validation = new Validation($_POST, $fieldsRules);
    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    }
    else
    {
        $categories->id = $_GET['cat'];
        $categories->valuesForInsert = $_POST;
        $categories->Update();
    }
}

if (isset($_GET['cat']))
{
    $categoryData = $categories->GetById();

    if (isset($_GET['obrisi']))
    {
        $categoryData = null;
    }
}
$categories->fields = "idKategorija,naziv,opis";
$allCat = $categories->GetAll();
