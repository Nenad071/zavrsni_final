<?php


class Predmeti extends Database {
    public $fields = "idPredmeta,naziv,slika,materijal,cena,opis,idKategorija";
    public $table = "predmeti";
    public $preparedValues = "?,?,?,?,?,?,?";
    public $primaryKey = "idPredmeta";
    public $filter = "";

}
