<div class="9u mobileUI-main-content">
    <div id="content">
<?php 
    $predmeti = new Predmeti();
    $predmeti->primaryKey="idKategorija";
    $predmeti->id= isset($_GET['cat'])?$_GET['cat']:null;

    $predmeti = $predmeti->GetById();

    $i=0;

    if(!is_null($predmeti) && (is_object($predmeti)||is_array($predmeti)))
    {
        if(is_array($predmeti))
        {
            foreach ($predmeti as $pred)
            {
?>
            <section class="heightSec">
                    <div class="post">
                        <h2><?= $pred->naziv?></h2>
                        <p class="aboveImg">
                            <b class="cena">Price:</b>
                            <b class="iznos">$<?= $pred->cena?></b>
                        </p>
                        <p class="opis">
                            <a href="#">
                                <img src="files/<?= $pred->slika?>" alt="" class="img-alignleft" style="width:225px;height:300px;">
                            </a><?= substr($pred->opis,0,958); ?>...
                        </p>
                        <?php
                            if(isset($_SESSION['user']))
                            {
                        ?>  
                        <p class="button-style">
                            <a href="index.php?page=8&idpredmeta=<?= $pred->idPredmeta ?>">Read Full Article</a>
                            <a href="procesess/dodaj.php?idPredmeta=<?= $pred->idPredmeta?>">Add To Cart</a>
                        </p>
                        <?php 
                            }
                        ?>
                    </div>
            </section>

<?php 
    $i++;
            }
        }
        else
        {                   
?>
            <section class="heightSec">
                <div class="post">
                    <h2><?= $predmeti->naziv?></h2>
                    <p class="aboveImg">
                        <b class="cena">Price:</b> 
                        <b class="iznos">$<?= $predmeti->cena?></b>
                    </p>
                    <p class="opis">
                        <a href="#">
                            <img src="files/<?= $predmeti->slika?>" alt="" class="img-alignleft" style="width:225px;height:300px;">
                        </a><?= substr($predmeti->opis,0,958); ?>...
                    </p>   
                    <?php
                        if(isset($_SESSION['user']))
                        {
                    ?>  
                        <p class="button-style">
                            <a href="index.php?page=8&idpredmeta=<?= $predmeti->idPredmeta ?>">Read Full Article</a>
                            <a href="procesess/dodaj.php?idPredmeta=<?= $predmeti->idPredmeta?>">Add To Cart</a>
                        </p>
                    <?php 
                        }
                    ?>

                </div>
            </section>
<?php 
        }
    }
    else 
    {
        $category = new Category();
        $category->fields = "naziv";
        $category->preparedValues="?";
        $category->id=$_GET['cat'];
        $category->filter = "and idKategorija = {$_GET['cat']}";
        $cat = $category->GetById();
        $noSubjectbyCat = "<p>There are no predmeti to fetch from category \"{$cat->naziv}\" !</p>";
        if (isset($cat->naziv)) echo $noSubjectbyCat;
    }
?>

    </div>
</div>
