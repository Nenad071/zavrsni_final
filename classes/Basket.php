<?php


class Basket extends Database
{
    
    public $fields = "idUser,content";# status i datum polja setovana u bazi na defaultnu frednost
    public $table = "basket";
    public $preparedValues = "?,?";
    public $primaryKey = "idBasket";
    public $filter = "";
    public $korpa;
    public $userId;


    public function __construct(User $user=null)
    {
        if (!is_null($user))
        {
            $this->korpa = $user->korpa;
            $this->userId = $user->idUser;
        }
    }
    
    public function InsertIntoBasket($status=null)
    {
        $arrayToInsert['idUser'] = $this->userId;
        $arrayToInsert['content'] = serialize($this->korpa);
        
        if ($status!==null)
        {
            $arrayToInsert['status']=$status;
            $this->fields="idUser,content,status";
            $this->preparedValues="?,?,?";
        }
        
        $this->valuesForInsert = $arrayToInsert;
        return $this->Insert();    
    }
    
    public function GetBasketByUser()
    {
            
            $this->primaryKey = "idUser";
            $this->id = $this->userId;
            $this->filter = "and status ='1'";  
            
            return $this->GetById();  
    }
}