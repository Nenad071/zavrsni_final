<?php

include 'procesess/processSearch.php';
$pretraga = "<p style='color:red;font-weight:bold;margin-bottom:30px;'>Nema rezultata pretrage za rec: \"$data\" </p>";

$i=0;
?>

<div class="9u mobileUI-main-content">
    <div id="content">
         
<?php 

if(!empty($predmetiSrc))
{
    $count = count($predmetiSrc);
    echo "<p style='color:red;font-weight:bold;margin-bottom:30px;'>Pronadjeno je {$count} rezultat(a) pretrage za trazenu rec: \"$data\" :</p>";
    foreach ($predmetiSrc as $pred)
    {        
?>
        <section class="heightSec">
            <div class="post">
                <h2><?= $pred->naziv?></h2>
                <p class="aboveImg">
                    <b class="cena">Price:</b> 
                    <b class="iznos">$<?= $pred->cena?></b>
                </p>
                <p class="opis">
                    <a href="#">
                        <img src="files/<?= $pred->slika?>" alt="" class="img-alignleft"  style="width:225px;height:300px;">
                    </a><?= substr($pred->opis,0,958)?>...
                </p>

                <p class="button-style">
                    <?php 
                        if (isset($_SESSION['user']))
                        {
                     ?>
                    <a href="index.php?page=8&idpredmeta=<?= $pred->idPredmeta ?>">Read Full Article</a>
                    <a href="procesess/dodaj.php?idPredmeta=<?= $pred->idPredmeta?>">Add To Cart</a>
                    <?php 
                        } 
                        else
                        {
                            echo "<p class='button-style'><a href='javascript:history.back()'>Back</a>";
                        }
                    ?>
                </p>
            </div>
        </section>
<?php 
    $i++;
    }
}

else
{    if(isset($stringError)) echo $stringError;
   else echo $pretraga; 
}
?>
    </div>
</div>
