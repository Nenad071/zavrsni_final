
<div class="9u mobileUI-main-content">
    <div id="content">
        
<?php

$mustLogin = '<p style="color:red;font-size:24px;margin: 100px 150px;">MORATE SE ULOGOVATI DA BISTE VIDELI SVOJ PROFIL.</p>'; 
$noOrders = '<p style="font-size:24px;margin: 100px 150px;">Niste do sada imali porudzbina. </p>';
$orders = '<p style="margin-top:30px;font-size:18px;font-weight: bold;">Vaše dosadašnje porudzbine: </p>';
$emptyBasket = "<p style='color:red;font-size:18px;font-weight: bold;'>Nemate aktivnu korpu. </p>";
$Basket = '<p style=";font-size:18px;font-weight: bold;">Vaša Korpa:</p>'; 


if (!is_null($user))
{
    $user->datumPristupa = date("d/M/Y H:i:s", strtotime($user->datumPristupa));
     
    if($user->idPristupa==1)
    {
?>
        
        <div style="margin:40px 0 40px 70px;">
            <h2>
                Dobro Dosli <?= $user->korisnickoIme; ?>
            </h2>
            <p>
                Vase Ime: <?= $user->ime; ?>
            </p>
            <p>
                Vase Prezime:<?= $user->prezime; ?>
            </p>
            
            <p>
                Vase Email:<?= $user->email; ?>
            </p>
            <p>
                Datum registracije:<?= $user->datumPristupa; ?>
            </p>
        </div>

<?php 
    }
}

$tableOrder= <<< TAB
<table class="tableLayout" style="margin-top:30px;">
    <tr>
        <th id="korpa">Id Korpe</th>
        <th id="datum">Datum</th>
        <th id="naz">Naziv</th>
        <th id="cena">Cena</th>
        <th id="dload">DownloadLink</th>
    </tr>
TAB;


$tableBasket=  <<< BASK
<table class="tableLayout" style="margin-top:30px;">
    <tr>
        <th id="aktiv">Aktiviraj</th>
        <th id="korpa">Id Korpe</th>
        <th id="datum">Datum</th>
        <th id="naz">Naziv</th>
        <th id="cena">Cena</th>
        
    </tr>
BASK;


   if (is_null($user))
    {
        echo $mustLogin;
    }
    else
    {
       $basket = new Basket();

       $basket->fields = "idBasket,content, status, datumPorudzbine";
       $basket->preparedValues= "?,?,?,?";
       $basket->primaryKey = "idUser";
       $basket->id= $user->idUser;
       $basket->filter= "and status = 1 ORDER BY datumPorudzbine DESC"; 
       $result1 = $basket->GetById();
       
       $basket = new Basket();

       $basket->fields = "idBasket,content, status, datumPorudzbine";
       $basket->preparedValues= "?,?,?,?";
       $basket->primaryKey = "idUser";
       $basket->id= $user->idUser;
       $basket->filter= "and status = 2 ORDER BY datumPorudzbine DESC"; 
       $result = $basket->GetById();
       
       
       if(empty($result))
       {
           echo $noOrders;
       }
       else
       {
            echo $orders;
            echo $tableOrder;
            if(is_array($result))
            {
                foreach ($result as $content)       
                {
                    $dateOfOrders  = date("d/M/Y H:i:s", strtotime($content->datumPorudzbine));
                    if($content->status==2) 
                    {
                        $i=0;
                        $content1 = unserialize($content->content);
                        foreach ($content1->nizProizvoda as $niz) 
                        {
                            echo "<tr>";
                            if ($i==0)
                            {
                ?>
                            <td id="korpa" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $content->idBasket?></td>
                            <td id="datum" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $dateOfOrders; ?></td>
                <?php
                            }
                            
                ?> 
                            
                            <td id="naz"><?= $niz->naziv?></td>
                            <td id="cena"><?= $niz->cena?></td>
                            <td id="dload"><a href="archive/<?= $niz->naziv ;?>.zip"><?= $niz->naziv; ?></a></td>
                <?php 
                        $i++;
                       
                        echo " </tr>";
                        }
                    }
                }   
            }
            else
            {
                if($result->status==2) 
                {
                    $dateOfOrder = date("d/M/Y H:i:s", strtotime($result->datumPorudzbine));
                    $i=0;
                    $content1 = unserialize($result->content);
                    foreach ($content1->nizProizvoda as $niz) 
                    {
                        echo "<tr>";
                        if ($i==0)
                        {
                ?>
                            <td id="korpa" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $result->idBasket?></td>
                            <td id="datum" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $dateOfOrder?></td>
                <?php
                        }
   
                ?> 
                            <td id="naz"><?= $niz->naziv?></td>
                            <td id="cena"><?= $niz->cena?></td>
                            <td id="dload"><a href="archive/<?= $niz->naziv ;?>.zip"><?= $niz->naziv; ?></a></td>
                <?php 
                        $i++;
                       
                        echo " </tr>";
                    }
                }
            }
                
 echo "</table>";

 $korpaPrazna = true;
 if ($result1)
 {
     if (is_object($result1))
     {
         if ($result1->status==1)
         {
              $korpaPrazna = false;  
         } 
     }
     else
     {
        foreach($result1 as $r)
        {
            if ($r->status==1)
            {
                 $korpaPrazna = false;
                 break;
            }
        }
     }

 }
 //else $korpaPrazna = false;
 
        if ($korpaPrazna)
        {
            echo $emptyBasket;
        }
        else
        {
            echo $Basket;     
            echo $tableBasket;
            if(is_array($result1))
            {
                foreach ($result1 as $content)
                {   
                    if($content->status==1) 
                    { 
                        $i=0;
                        $content1 = unserialize($content->content);
                        foreach ($content1->nizProizvoda as $niz) 
                        {
                            echo "<tr>";
                            if ($i==0)
                            {
                ?>
                            <td id="dload" rowspan="<?= count($content1->nizProizvoda); ?>">
                                <a href= "<?= _WEB_PATH.PROCESS_DIR; ?>Order.php">Aktiviraj Korpu</a>
                            </td>
                            <td id="korpa" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $content->idBasket?></td>
                            <td id="datum" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $content->datumPorudzbine?></td>
                <?php
                            }
                ?> 
                            <td id="naz"><?= $niz->naziv?></td>
                            <td id="cena"><?= $niz->cena?></td>      
                <?php 
                        $i++;
                        echo " </tr>";
                        }
                    }
                }    
            }
            else
            {
                if($result1->status==1) 
                {
                    $dateOfBasket = date("d/M/Y H:i:s", strtotime($result1->datumPorudzbine));
                    $i=0;
                    $content1 = unserialize($result1->content);
                    foreach ($content1->nizProizvoda as $niz) 
                    {
                        echo "<tr>";
                        if ($i==0)
                        {
                ?>
                            <td id="dload" rowspan="<?= count($content1->nizProizvoda); ?>">
                                <a href="<?= _WEB_PATH.PROCESS_DIR; ?>Order.php">Aktiviraj Korpu</a>
                            </td>
                            <td id="korpa" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $result1->idBasket?></td>
                            <td id="datum" rowspan="<?= count($content1->nizProizvoda); ?>"><?= $dateOfBasket ?></td>
                <?php
                        }
                ?>  
                            <td id="naz"><?= $niz->naziv?></td>
                            <td id="cena"><?= $niz->cena?></td>         
                <?php 
                        $i++;
                        echo " </tr>";
                    }
                }
            } 
            echo "</table>";
        }
    }
}

 echo "</div>"
. "</div>";

