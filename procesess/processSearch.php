<?php

$rules = array
(
    "src" =>array("required"=>"","min"=>"2")
);

$urlSrc = urldecode($_GET['src']);
//$urlSrc = $_GET['src']; radi i bez urldecode();

$_GET['src'] = $urlSrc; 

$predmeti=new Predmeti();
$predmeti->fields = "idPredmeta,naziv,slika,cena,opis";
$predmeti->preparedValues="?,?,?,?,?";
$predmeti->table="predmeti";
$predmeti->primaryKey ="idPredmeta";

if (isset($_GET['search']))
{
    $data="";
    $predmetiSrc=null;
    $validation = new Validation($_GET, $rules);
    $result = $validation->validate();
    
    if ($result["error"]===true)
    {
        $stringError = "";
        foreach($result["messages"] as $k=>$v)
        {
            foreach($v as $key=>$value)
            {
                $stringError.= "<span style='color:red'>".$value."</span> <br>";
            }
        }
    }
    else 
    {
        if(isset($urlSrc)&& !empty($urlSrc))
        {
            $data = inputFilter($urlSrc);
            $predmeti->filter = "and opis like '%{$data}%' or naziv like '%{$data}%'";
            $predmetiSrc = $predmeti->GetAll();
        }
    }
}