<?php


class Validation 
{

    public $errorMessages = array
    (
        "required" => "The field #field# is required",
        "min" => "The field #field# must contain at least #numchars# characters",
        "max" => "The field #field# cannot contain more than #numchars# characters",
        "alpha" => "The field #field# can only contain letters",
        "alphanumeric" => "The field #field# can only contain letters and numbers",
        "number" => "The field #field# can only contain number",
        "email" => "The field #field# has to be valid email address",
        "confirm"=> "The field #field# has to be the same as field #field1#",
        "minnumber"=>"Minimum value of the  field #field# is  #number#"
    );
    private $data = array();
    private $rules = array();
    private $validationMessages = array();

    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }

    private function checkRequired($key)
    {
        if ($this->data[$key] == "" || (is_numeric($this->data[$key]) && $this->data[$key]<1))
        {
            $this->validationMessages[$key][] = str_replace("#field#", $key, $this->errorMessages["required"]);
        }
    }

    private function checkAlpha($key)
    {

        if (!preg_match('/^\p{L}[\p{L} _.-]+$/u', $this->data[$key])) 
        {
            $this->validationMessages[$key][] = str_replace("#field#", $key, $this->errorMessages["alpha"]);
        }
    }

    
    private function checkAlphanumeric($key)
    {

        if(!preg_match('/[\p{L}\p{N}[\p{L} _.-]+$/u', $this->data[$key]))
        {
            $this->validationMessages[$key][]=str_replace("#field#",$key,$this->errorMessages["alphanumeric"]);
        }

    }

    private function checkNumber($key)
    {
        if (!is_numeric($this->data[$key]))
        {
            $this->validationMessages[$key][] = str_replace("#field#", $key, $this->errorMessages["number"]);
        }
    }

    private function checkMin($key)
    {
        if (strlen($this->data[$key]) < $this->rules[$key]["min"])
        {
            $this->validationMessages[$key][] = str_replace(array("#field#", "#numchars#"), array($key, $this->rules[$key]["min"]), $this->errorMessages["min"]);
        }
    }

    private function checkMax($key)
    {
        if (strlen($this->data[$key]) > $this->rules[$key]["max"])
        {
            $this->validationMessages[$key][] = str_replace(array("#field#", "#numchars#"), array($key, $this->rules[$key]["max"]), $this->errorMessages["max"]);
        }
    }

    private function checkEmail($key)
    {
        if (!filter_var($this->data[$key], FILTER_VALIDATE_EMAIL))
        {
            $this->validationMessages[$key][] = str_replace("#field#", $key, $this->errorMessages["email"]);
        }
    }
    
    private function checkConfirm($key)
    {
        $keyArray = explode("_", $key);
        if ($this->data[$key] !== $this->data[$keyArray[0]])
        {
            $this->validationMessages[$key][] = str_replace(array( "#field#","#field1#"), array($key,$keyArray[0]) , $this->errorMessages["confirm"]);
        }
    }
    
    private function checkMinnumber($key)
    {
        if ($this->data[$key] < $this->rules[$key]["minnumber"])
        {
            $this->validationMessages[$key][] = str_replace(array( "#field#","#number#"), array($key,$this->rules[$key]["minnumber"]), $this->errorMessages["minnumber"]);
        }
    }
    

    public function validate() 
    {
        foreach ($this->rules as $k => $v)
        {
            foreach ($v as $key => $value) 
            {
                $key = ucfirst($key);
                $methodname = "check" . $key;
                $this->$methodname($k);
             }
        }
        if (empty($this->validationMessages))
        {
            return array("error" => false, "messages" => "");
        } 
        else 
        {
            return array("error" => true, "messages" => $this->validationMessages);
        }
    }

}


