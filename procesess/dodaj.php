<?php

include "../config.php";

if(!isset($_SESSION['user']))
{
    die( "Morate biti ulogovani");
}

if (isset($_GET['ponisti']))
{
    $basket = new Basket($user);
    $basket->fields= "idBasket,idUser,content";
    $basket= $basket->GetBasketByUser();
   
    if(is_object($basket))
    {
        $basket->primaryKey="idBasket";   
        $basket->id=$basket->idBasket;
        $basket->Delete();
    }
    $user->RemoveCart();
//    $user->SetSession($user);
//    header('Location:' . $_SERVER['HTTP_REFERER']);
}
else
{
    if (isset($_GET['idPredmeta']) && !isset($_GET['obrisi']))
    { 
        $dodaj = false;    

        $proizvod = new Predmeti();
        $proizvod->id= $_GET['idPredmeta'];
        $proizvod = $proizvod->GetById();

        if(!isset($_SESSION['korpa']))
        { 
            $dodaj = true;
            $korpa = new Korpa();   
        }
        else 
        {
            $korpa= Korpa::UnserializeKorpa();
            $dodaj = true;
            foreach($korpa->nizProizvoda as $np)
            {
                if ($np->idPredmeta == $_GET['idPredmeta'])
                {
                    $dodaj = false;
                }
            }
        }

        if ($dodaj)
        {
            $korpa->addToCart($proizvod);
            $korpa->SetSession($korpa);  
        }
    }
        
    if (isset($_GET['idPredmeta']) && isset($_GET['obrisi']))
    {
        $korpa = Korpa::UnserializeKorpa();
        $korpa->RemoveItem($_GET['idPredmeta']);
        $korpa->SetSession($korpa); 
    }

    $user= User::UnserializeUser();
    $user->AddCart($korpa);
//        $user->SetSession($user);
//        header('Location:' . $_SERVER['HTTP_REFERER']);
}

    $user->SetSession($user);
    header('Location:' . $_SERVER['HTTP_REFERER']);


    


