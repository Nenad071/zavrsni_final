
<?php 

    $lastItems = new Predmeti();
    $lastItems->fields = "idPredmeta,naziv,datumUnosa";
    $lastItems->table = "predmeti";
    $lastItems->preparedValues = "?,?,?";
    $lastItems->primaryKey = "idPredmeta";
    $lastItems->id = (isset($_GET['idPredmeta']))? $_GET['idPredmeta']: null;
    $lastItems->filter = "order by datumUnosa desc limit 5";
    $list = $lastItems->GetAll();

    
    $randItems = new Predmeti();
    $randItems->fields = "idPredmeta,naziv,slika,materijal,cena,opis,idKategorija,datumUnosa";
    $randItems->table = "predmeti";
    $randItems->preparedValues = "?,?,?,?,?,?,?,?";
    $randItems->primaryKey = "idPredmeta";
    $randItems->filter = "order by rand() limit 5";
    $list1 = $randItems->GetAll();

?>
    </div>
</div>
<div class="5grid-layout">
    <div class="row" id="footer-content">
        <div class="6u" id="box1">
            <section>
                <h2>O NAMA</h2>
                <div>
                    <p><img src="images/footer2.jpg" alt="" width="180" height="120" class="imgleft">
                        WEB ARCHIVES predstavlja svojevrsni online zbornik najekskluzivnijih dokumenata iz 
                        najvećih svetskih nacionalnih arhiva. Osnovna tematika jesu globalni konflikti u XX veku.
                        WEB ARCHIVES  svim zainteresovanim pojedincima pruža mogućnost u uvid u sva najznačajnija 
                        svetska dešavanja u prošlom veku. Namena ovog sajta je  prodaja dokumenata ili delova
                        dokumenata svojim korisnicima.
                        Ovde mozete pronaci dokumente iz britanskog i americkog nacionalnog arhiva. 
                        Navodimo samo neke od interesantnih tema: strategija, politika, bitke, obaveštajni rad,
                        zavere, agencije, planovi, istorijske ličnosti itd..
                        
<!--                        In posuere eleifend odio. Quisque semper augue mattis wisi. Maecenas ligula. 
                        Pellentesque viverra vulputate enim. Aliquam erat volutpat. Pellentesque tristique ante 
                        ut risus. Quisque dictum. Integer nisl risus, sagittis convallis, rutrum id,
                        elementum congue, nibh. Suspendisse dictum porta lectus. Donec placerat odio vel elit. 
                        Nullam ante orci, pellentesque eget, tempus quis, ultrices in, est. 
                        Curabitur sit amet nulla. Nam in massa. Sed vel tellus. Curabitur sem urna, 
                        consequat vel, suscipit in, mattis placerat, nulla. Sed ac leo. 
                        Pellentesque imperdiet. Etiam neque. Vivamus consequat lorem at nisl.
                        Nullam non wisi a sem semper eleifend. Donec mattis libero eget urna. 
                        Duis pretium velit ac mauris. Proin eu wisi suscipit nulla suscipit interdum. 
                        Aenean lectus lorem, imperdiet at, ultrices eget, ornare et, wisi. -->
                    </p>
                </div>
            </section>
        </div>
        <div class="3u" id="box2">
            <section>
                <h2>Najnoviji dokumenti: </h2>
                <ul class="style4">
    <?php 
        foreach ($list as $item)
        {
            echo "<li><a href='?page=8&idpredmeta=" . $item->idPredmeta . "'> $item->naziv</a></li>";
        }
    ?>
                </ul>
            </section>
        </div>
        <div class="3u" id="box3">
            <section>
                <h2>Po slucajnom izboru: </h2>
                <ul class="style4">                    
    <?php 
        foreach ($list1 as $item1)
        {
            echo "<li><a href='?page=8&idpredmeta=" . $item1->idPredmeta . "'> $item1->naziv</a></li> ";
        }
    ?>
                </ul>
            </section>
        </div>
    </div>
</div>
